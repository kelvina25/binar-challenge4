import React from "react";

// import 'swiper/dist/js/swiper.js'

import "swiper/swiper.min.css";
import "./assets/boxicons-2.0.7/css/boxicons.min.css";
import "./App.scss";


import {  createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";

import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Catalog from "./pages/Catalog";
import Home from "./pages/Home";
import Detail from "./pages/detail/Detail";

import Routing from "./config/Routing";

function App() {
  const Layout = () => {
    return (
      <>
        <Header />
        <Outlet />
        <Footer />
      </>

      
    );
  };
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout />,
      children: [
        {
          path: "/",
          element: <Home />,
        },
        
        {
          path: '/:category',
          element: <Catalog />,
        },

        {
          path: '/:category/:id',
          element: <Detail />
        },

        {
          path: '/:category/search/:keyword',
          element: <Catalog />
        }
      ],  
    },
  ]);
  return (
    <div>
      <RouterProvider router={router} />
      
    </div>
// < BrowserRouter>
//   <Routes>
//     <Route
//       path="/"
//       animate={true}
//       element={
//         <React.Fragment>
//           <Header />

//           <Footer />
//           <Routing />
//         </React.Fragment>
//       }
//     />

//     <Route
//       path="/Movie"
//       element={
//         <React.Fragment>
//           <Catalog />
//         </React.Fragment>
//       }
//     />
//   </Routes>
// </>
);
}


export default App;
// {/* <BrowserRouter>
// <Header />
// <Routes>
//   <Route path="/" element={<Home />}/>
//   <Route path="/Catalog" element={<Catalog />}/>
//   <Route path="/Detail" element={<Detail />}/>
// </Routes>
// <Footer />

// </BrowserRouter>   */}
